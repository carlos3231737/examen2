const btnGen = document.getElementById('gen');
const btnLim = document.getElementById('limpiar');
btnGen.addEventListener('click', function(){
    generar();
})

function generar(){
    var arr = [];
    var bb = 0, ni = 0, ado = 0, adu = 0, an = 0;
    let total = '';
    var pro = 0;
    for(x = 0; x<100; x++)
    {
        arr[x] = Math.floor(Math.random() * 90);
        total += arr[x] + ", ";
        switch(true){
            case (arr[x] < 4):
                bb++;
                break;
            case (arr[x] < 13):
                ni++;
                break;
            case (arr[x] < 18):
                ado++;
                break;
            case (arr[x] < 61):
                adu++;
                break;
            default:
                an++;
                break;
        }
        pro += arr[x];
    }
    pro = pro / 100;
    document.getElementById('cadena').innerText = total;
    document.getElementById('tabla').innerText = "Bebés: " +bb +"\nNiños: " +ni +"\nAdolescentes: " +ado +"\nAdultos: " +adu +"\nAncianos: " +an +"\nEdad promedio: " +pro;
}

btnLim.addEventListener('click', function(){
    document.getElementById('cadena').innerText = '';
    document.getElementById('tabla').innerText = '';

})