const btnCal = document.getElementById("btnCal");
const btnLim = document.getElementById("btnLim");
btnCal.addEventListener('click', function() {
    let pre = document.getElementById('precio').value;
    const tipo = document.getElementById('tviaje').value;
    if(tipo == 2) {
        pre *= 1.8;
    }
    document.getElementById("stotal").value = pre;
    const imp = pre *0.16;
    document.getElementById('impuesto').value = imp;
    const total = pre + imp;
    document.getElementById('tpagar').value = total;
})

btnLim.addEventListener('click', function() {
    document.getElementById('nboleto').value = '';
    document.getElementById('ncliente').value = '';
    document.getElementById('destino').value = '';
    document.getElementById('tviaje').value = 1;
    document.getElementById('precio').value = '';
    document.getElementById('stotal').value = '';
    document.getElementById('impuesto').value = '';
    document.getElementById('tpagar').value = '';
})